const memoization = require('./memoizaton');
const expect = require('chai').expect;

// hint: use https://sinonjs.org/releases/v6.1.5/fake-timers/ for faking timeouts

describe('memoization', function () {
    it('should memoize function result', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;
        
        const memoized = memoization.memoize(testFunction,(key)=>key, 1000);
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        returnValue = 10;

        // TODO currently fails, should work after implementing the memoize function
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);
    });

    // TODO additional tests required

    
});

    // TODO additional tests required
    describe('memoization', function () {
        it('should memoize function result', () =>{
            let returnValue = 5;
            const testFunction =  (key) => returnValue+10;
            
            const memoized = memoization.memoize(testFunction,(key)=>key, 1000);
            expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(15);
    
            returnValue = 10;
    
            // TODO currently fails, should work after implementing the memoize function
            expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(15);
        });
    
        // TODO additional tests required
    
        
    });
    

     function someFunction(key, otherParameter) {
      return Date.now();
    }
    
     const memoized = memoization.memoize(someFunction, (key) => key, 5000)
    
     // there is no value for the key provided therefore the first call should directly call the provided function
     // cache the result and return the value
     const result = memoized('c544d3ae-a72d-4755-8ce5-d25db415b776', 'not relevant for the key'); // result = 1534252012350
    
     // because there was no timeout and only the first argument is used as key this call should return the memorized value from the
     // first call
     const secondResult = memoized('c544d3ae-a72d-4755-8ce5-d25db415b776', 'different parameter'); // secondResult = 1534252012350
    
     // after 5000 ms the value is not valid anymore and the original function should be called again
     const thirdResult = memoized('c544d3ae-a72d-4755-8ce5-d25db415b776', 'not relevant for the key'); // thirdResult = 1534252159271